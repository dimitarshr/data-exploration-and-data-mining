library(ggplot2)

# Load the dataset
dataset <- read.csv("./dataset.csv")

# X = Location, Y = Overall score
ggplot(dataset, aes(x=Location, y = so, fill = Location)) + geom_boxplot() + labs(title="Overall score based on the location", x="Location", y="Overall score")  + scale_fill_manual(values=c("#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00")) + theme_bw() + guides(fill = FALSE) + theme(plot.title=element_text(size=25, face="bold"), axis.text.x=element_text(size=20), axis.text.y=element_text(size=20), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20))
#######################################################

# X = Age, Y = Distance score
dataset$Age <- factor(dataset$Age, levels = c("T", "U", "L", "O"))
ggplot(dataset, aes(x=Age, y=sd, fill=Age)) + geom_boxplot()  + labs(title="Distance score based on the age", x="Age", y="Distance score")  + theme_bw() + scale_fill_manual(values=c("#ffffb2", "#fecc5c", "#fd8d3c", "#e31a1c")) + guides(fill = FALSE)  + theme(plot.title=element_text(size=25, face="bold"), axis.text.x=element_text(size=20), axis.text.y=element_text(size=20), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20))
#######################################################

# X = Age, Y = Jump score
# NB - Only for females
femaleOnly = dataset[dataset$Gender=="F",]
# Rearrange the age groups in a natural order
femaleOnly$Age <- factor(femaleOnly$Age, levels = c("T", "U", "L", "O"))
ggplot(femaleOnly, aes(x=Age, y = sj, fill = Age)) + geom_boxplot()  + labs(title="Jump score based on the age for females only", x="Age", y="Jump score")  + theme_bw() + scale_fill_manual(values=c("#ffffb2", "#fecc5c", "#fd8d3c", "#e31a1c")) + guides(fill = FALSE) + theme(plot.title=element_text(size=25, face="bold"), axis.text.x=element_text(size=20), axis.text.y=element_text(size=20), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20)) + scale_x_discrete(labels = c('Teenagers','Under 25\n(but over 20)','Late 20s', 'Over 30'))
#######################################################

# X = Sprint score, Y = Throw score
# NB - Only location A and C - say wht there are two
onlyLocationAandC = dataset[dataset$Location=="A" | dataset$Location=="C",]
ggplot(onlyLocationAandC, aes(x=ss, y = st, color = Location)) + geom_smooth(show.legend = FALSE) + geom_point(show.legend = FALSE)  + labs(title="Sprint score related to throw score for location A and C only", x="Sprint score", y="Throw score")  + theme_bw() + facet_wrap( ~ Location, ncol=2) + theme(plot.title=element_text(size=25, face="bold"), axis.text.x=element_text(size=20), axis.text.y=element_text(size=20), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20))
#######################################################

# X = Distance score, Y = Jump score
# NB - Only location B, Males - Make it as one
malesAtLocationB = dataset[dataset$Location=="B" & dataset$Gender=="M",]
ggplot(malesAtLocationB, aes(x=sd, y = sj)) + geom_smooth(show.legend = FALSE)+ geom_point(show.legend = FALSE)  + labs(title="Distance score related to jump score for location B and males only", x="Distance score", y="Jump score")  + theme_bw() + theme(plot.title=element_text(size=25, face="bold"), axis.text.x=element_text(size=20), axis.text.y=element_text(size=20), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20))
#######################################################

# X = Sprint score, Y = Jump score
# NB - Only location E, Females and Teenagers
femaleTeenagersAtLocationE = dataset[dataset$Gender=="F" & dataset$Location=="E" & dataset$Age=="T",]
ggplot(femaleTeenagersAtLocationE, aes(x=ss, y = sj)) + geom_point() + geom_smooth() + labs(title="Sprint score related to jump score for location E, \nfemales and teenages only", x="Sprint score", y="Jump score") + theme_bw()  + theme(plot.title=element_text(size=22, face="bold"), axis.text.x=element_text(size=20), axis.text.y=element_text(size=20), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20))
#######################################################

# X = Throw score, Y = Overall score - The throw doesn't affect the women overall score
ggplot(dataset, aes(x=st, y = so, color=Gender)) + geom_smooth(method=lm)  + labs(title="Throw score related to overall score based on the gender", x="Throw score", y="Overall score") + theme_bw()  + theme(plot.title=element_text(size=22, face="bold"), axis.text.x=element_text(size=20), axis.text.y=element_text(size=20), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20))
#######################################################

## Using facet_wrap to thoroughly explore the data. This section is only experimental, however, extremely useful.
gg <- ggplot(dataset, aes(x=ss, y=sj, color=Location)) + geom_point()
gg1 <- gg + theme(plot.title=element_text(size=5, face="bold"),
                  axis.text.x=element_text(size=5),
                  axis.text.y=element_text(size=5),
                  axis.title.x=element_text(size=5),
                  axis.title.y=element_text(size=5))
gg1 + facet_wrap( ~ Age, ncol=3)
gg1 + facet_wrap(Location ~ Age)
#######################################################
