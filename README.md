# Data Exploration

The data exploration is done on a dataset with the following description:
Athletes from 5 locations have been competing in a multi-discipline competition. The disciplines are categorised as sprints, jumps, throws and distance. In each discipline, athletes have been assigned a score out of 100. In addition, they have been assigned an overall score, again out of 100. Their age, as one of four bands, has been recorded, along with their gender. In summary, each entry in the dataset contains:

* Gender (can be M or F)
* Location (can be A, B, C, D or E)
* Age. The age is in four bands, Teenager, Under 25 (but over 20), Late 20s, and Over 30.
* Sprints. Score out of 100.
* Jumps. Score out of 100.
* Throws. Score out of 100.
* Distance. Score out of 100.
* Overall. Score out of 100

The goal of the data exploration was to identify any trends, relationships, or other interesting information in the dataset. Further details can be found in **the report in the Data Exploration folder**.

---

# Data Mining

The data mining is done on a dataset that contains historic observations on 10 variables (attributes) for 1000 past applications for credit. Each applicant was given a rate of “good” (700 cases) or “bad” (300 cases) credit. Based on the applicant’s profile, a bank can make reasonable decisions about whether or not to award a loan. The goal here was to achieve the following three tasks:

* Prepare and clean the data for analysing using **Open Refine**.
* Analyse the data with appropriate techniques/algorithms such as Classification, Regression, Association and Clustering algorithms. This stage was further expanded to indetify interesting patterns, associations and rules. This task was achieved using **Weka**.
* Based on the analysis an overall conclusion had to be summerised.

Further details can be found in **the report in the Data Mining folder**.